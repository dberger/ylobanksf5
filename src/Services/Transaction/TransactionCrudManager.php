<?php

namespace App\Services\Transaction;

use App\Entity\Transaction\Transaction;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;

/**
 * Class TransactionFactory.
 */
final class TransactionCrudManager
{
    /**
     * @var EntityManager
     */
    private EntityManager $entityManager;

    /**
     * TransactionCrudManager constructor.
     *
     * @param EntityManager $entityManager
     */
    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @param Transaction $transaction
     *
     * @return Transaction
     *
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function save(Transaction $transaction): Transaction
    {
        $this->entityManager->persist($transaction);
        $this->entityManager->flush();

        return $transaction;
    }
}
