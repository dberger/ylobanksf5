# YloBankSF5

## Description

*YloBankSF5* is banking API app, written using [ApiPlatform](https://api-platform.com/) on [Symfony 5](https://symfony.com/).

It's a Bankin'app like and a rework of my first project [YloBank](https://gitlab.com/dberger/ylobank)

## Installation
- set .env configuration
 - `composer install`
 - `make start`
 - `make db`
 
 See `make help` to display available commands.

## Tests
*WIP*
