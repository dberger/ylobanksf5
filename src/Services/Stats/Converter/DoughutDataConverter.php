<?php

namespace App\Services\Stats\Converter;

use App\Repository\TransactionRepository;

/**
 * Class DoughutDataConverter.
 */
final class DoughutDataConverter implements IDataConverter
{
    /**
     * @param array $data
     *
     * @return array
     */
    public function convert(array $data): array
    {
        $labels = [];
        $fullLabels = [];
        $items = [];

        foreach ($data as $value) {
            $labels[] = $value[TransactionRepository::KEY_NAME];
            $items[] = $value[TransactionRepository::KEY_SUM];
            $fullLabels[] = $value[TransactionRepository::KEY_NAME].' ('.$value[TransactionRepository::KEY_SUM].' €)';
        }

        return ['labels' => $labels, 'items' => $items, 'fullLabels' => $fullLabels];
    }
}
