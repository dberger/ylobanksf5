<?php

namespace App\Services\Account;

use App\Entity\Account\Account;
use App\Entity\User\User;
use App\Repository\AccountRepository;
use App\Services\AbstractServices\BaseSearchProvider;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;

/**
 * Class AccountTypeSearchProvider.
 */
final class AccountSearchProvider extends BaseSearchProvider
{
    /**
     * AccountTypeSearchProvider constructor.
     *
     * @param AccountRepository $accountRepository
     */
    public function __construct(AccountRepository $accountRepository)
    {
        parent::__construct($accountRepository);
    }

    /**
     * @param User $user
     * @param int  $accountId
     *
     * @return Account|null
     */
    public function getAccountWithUserConstraint(User $user, int $accountId): ?Account
    {
        /** @var AccountRepository $repo */
        $repo = $this->entityRepository;
        try {
            return $repo->getAccountWithUserConstraint($user, $accountId)->getSingleResult();
        } catch (NoResultException $e) {
            return null;
        } catch (NonUniqueResultException $e) {
            return null;
        }
    }
}
