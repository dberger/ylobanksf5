<?php

namespace App\Services\Stats\Converter;

/**
 * Interface IDataConverter.
 */
interface IDataConverter
{
    /**
     * @param array $data
     *
     * @return array
     */
    public function convert(array $data): array;
}
