EXEC_PHP        = php
SYMFONY         = $(EXEC_PHP) bin/console
VENDOR			= ./vendor

db: ## Reset the database and load fixtures - add .env
db:
	-$(SYMFONY) doctrine:database:drop --if-exists --force
	-$(SYMFONY) doctrine:database:create --if-not-exists
	$(SYMFONY) doctrine:migrations:migrate --no-interaction --allow-no-migration
	$(SYMFONY) d:f:l --append

start: ## Start container
start:
	symfony server:start -d

stop: ## Stop container
stop:
	symfony server:stop

php-cs-fixer-fix: ## php-cs-fixer (http://cs.sensiolabs.org)
	$(EXEC_PHP) ./$(VENDOR)/bin/php-cs-fixer fix --allow-risky=yes src

.DEFAULT_GOAL := help
help:
	@grep -E '(^[a-zA-Z_-]+:.*?##.*$$)|(^##)' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[32m%-30s\033[0m %s\n", $$1, $$2}' | sed -e 's/\[32m##/[33m/'
.PHONY: help
