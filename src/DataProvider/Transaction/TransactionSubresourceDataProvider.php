<?php

namespace App\DataProvider\Transaction;

use ApiPlatform\Core\DataProvider\RestrictedDataProviderInterface;
use ApiPlatform\Core\DataProvider\SubresourceDataProviderInterface;
use App\Entity\Transaction\Transaction;
use App\Entity\User\User;
use App\Repository\TransactionRepository;
use Generator;
use InvalidArgumentException;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

/**
 * Class TransactionSubresourceDataProvider.
 */
final class TransactionSubresourceDataProvider implements SubresourceDataProviderInterface, RestrictedDataProviderInterface
{
    /**
     * @var TransactionRepository
     */
    private TransactionRepository $transactionRepository;

    /**
     * @var TokenStorageInterface
     */
    private TokenStorageInterface $tokenStorage;

    public function __construct(TransactionRepository $transactionRepository, TokenStorageInterface $tokenStorage)
    {
        $this->transactionRepository = $transactionRepository;
        $this->tokenStorage = $tokenStorage;
    }

    /**
     * @param string $resourceClass
     * @param string|null $operationName
     * @param array $context
     *
     * @return bool
     */
    public function supports(string $resourceClass, string $operationName = null, array $context = []): bool
    {
        return Transaction::class === $resourceClass;
    }

    /**
     * @param string $resourceClass
     * @param array $identifiers
     * @param array $context
     * @param string|null $operationName
     *
     * @return array|Generator|object|null
     */
    public function getSubresource(
        string $resourceClass,
        array $identifiers,
        array $context,
        string $operationName = null
    ): array
    {
        $sortedIdentifiers = $this->getSortedIdentifiers($identifiers);

        if (null === $sortedIdentifiers['accountId']) {
            throw new InvalidArgumentException('You must use an accountId.');
        }

        /** @var User $user */
        $user = $this->tokenStorage->getToken()->getUser();

        if (
            !$user instanceof User
            || ($user instanceof User && !$user->hasAccountId($sortedIdentifiers['accountId']))
            || (null !== $sortedIdentifiers['userId'] && $user instanceof User && $user->getId() !== $sortedIdentifiers['userId'])) {
            throw new AccessDeniedException('You must be logged in to access this subresource or you don\'t have permissions to access it.');
        }

        $query = $this
            ->transactionRepository
            ->createQueryBuilder('t')
            ->innerJoin('t.account', 'a')
            ->andWhere('a.user = :user')
            ->andWhere('a.id = :id')
            ->setParameter('user', $user)
            ->setParameter('id', $sortedIdentifiers['accountId']);

        return $query->getQuery()->getResult();
    }

    /**
     * @param array $identifiers
     *
     * @return array
     */
    private function getSortedIdentifiers(array $identifiers): array
    {
        $accountId = $identifiers['accounts']['id'] ?? null;
        $userId = $accountId ? $identifiers['id']['id'] ?? null : null;

        if (!$accountId) {
            $accountId = $identifiers['id']['id'] ?? null;
        }

        return [
            'userId' => $userId,
            'accountId' => $accountId,
        ];
    }
}
