<?php

namespace App\Repository;

use App\Entity\Transaction\TransactionType;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query;
use Doctrine\Persistence\ManagerRegistry;

/**
 * Class TransactionTypeRepository.
 */
final class TransactionTypeRepository extends ServiceEntityRepository
{
    /**
     * TransactionTypeRepository constructor.
     *
     * @param ManagerRegistry $registry
     */
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TransactionType::class);
    }

    /**
     * Return only the objects that are for the debit form.
     *
     * @param string $type
     *
     * @return Query
     */
    public function getTypes(string $type = null): Query
    {
        $qb = $this->createQueryBuilder('tt');
        if (in_array($type, [TransactionType::SEARCH_CREDIT, TransactionType::SEARCH_DEBIT], true)) {
            $qb
                ->andWhere('tt.code IN (:codes)')
                ->setParameter('codes', array_keys(TransactionType::SEARCH_DEBIT === $type ? TransactionType::getDebitTypes() : TransactionType::getCreditTypes()));
        }

        return $qb->getQuery();
    }
}
