<?php

namespace App\Repository;

use App\Entity\Account\Account;
use App\Entity\User\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query;
use Doctrine\Persistence\ManagerRegistry;

/**
 * Class AccountRepository.
 */
final class AccountRepository extends ServiceEntityRepository
{
    /**
     * AccountRepository constructor.
     *
     * @param ManagerRegistry $registry
     */
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Account::class);
    }

    /**
     * @param User $user
     * @param int  $accountId
     *
     * @return Query
     */
    public function getAccountWithUserConstraint(User $user, int $accountId): Query
    {
        return $this
            ->createQueryBuilder('a')
            ->andWhere('a.user = :user')
            ->andWhere('a.id = :account_id')
            ->setParameter('user', $user)
            ->setParameter('account_id', $accountId)
            ->getQuery();
    }
}
