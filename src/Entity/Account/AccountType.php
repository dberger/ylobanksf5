<?php

namespace App\Entity\Account;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class AccountType.
 *
 * @ORM\Entity(repositoryClass="App\Repository\AccountTypeRepository")
 */
class AccountType
{
    const ACC_TYPE_LIVRET_JEUNE = 'livret_jeune';
    const ACC_TYPE_LIVRET_DEV_DURABLE = 'livret_dev_durable';
    const ACC_TYPE_CPT_CHEQUE = 'cpt_cheque';

    const PERCENT_LIVRET_JEUNE = 1.75;
    const PERCENT_LIVRET_DEV_DURABLE = 0.75;
    const PERCENT_CPT_CHEQUE = 0;

    const LIBELLE_LIVRET_JEUNE = 'Livret Jeune';
    const LIBELLE_LIVRET_DEV_DURABLE = 'Livret de dév durable & solidaire';
    const LIBELLE_CPT_CHEQUE = 'Compte Courant';

    const ARRAY_CODE_LIBELLE = [
        self::ACC_TYPE_LIVRET_JEUNE => self::LIBELLE_LIVRET_JEUNE,
        self::ACC_TYPE_LIVRET_DEV_DURABLE => self::LIBELLE_LIVRET_DEV_DURABLE,
        self::ACC_TYPE_CPT_CHEQUE => self::LIBELLE_CPT_CHEQUE,
    ];

    /**
     * @var int
     *
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Groups({"read_account"})
     */
    private int $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     * @Assert\NotNull()
     */
    private string $name;

    /**
     * @var string
     *
     * @ORM\Column(name="code", type="string", length=255)
     * @Assert\NotNull()
     * @Groups({"read_account"})
     */
    private string $code;

    /**
     * @var float
     *
     * @ORM\Column(name="interest_percentage", type="decimal", precision=7, scale=2, nullable=true)
     * @Assert\NotNull()
     */
    private float $interestPercentage;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     *
     * @return AccountType
     */
    public function setName(string $name): AccountType
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return float
     */
    public function getInterestPercentage(): float
    {
        return $this->interestPercentage;
    }

    /**
     * @param float $interestPercentage
     *
     * @return AccountType
     */
    public function setInterestPercentage(float $interestPercentage): AccountType
    {
        $this->interestPercentage = $interestPercentage;

        return $this;
    }

    /**
     * @return string
     */
    public function getCode(): string
    {
        return $this->code;
    }

    /**
     * @param string $code
     *
     * @return AccountType
     */
    public function setCode(string $code): AccountType
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Return an array of account's code as key with the linked percentage of interest
     * Used for migrations.
     *
     * @return array
     */
    public static function getAccountsConstants(): array
    {
        return [
            self::ACC_TYPE_LIVRET_DEV_DURABLE => self::PERCENT_LIVRET_DEV_DURABLE,
            self::ACC_TYPE_LIVRET_JEUNE => self::PERCENT_LIVRET_JEUNE,
            self::ACC_TYPE_CPT_CHEQUE => self::PERCENT_CPT_CHEQUE,
        ];
    }

    /**
     * Return the correct libelle of an account type with a code.
     *
     * @param string $code
     *
     * @return string
     */
    public static function getAccountTypeLibelleWithCode(string $code): ?string
    {
        return self::ARRAY_CODE_LIBELLE[$code] ?? null;
    }
}
