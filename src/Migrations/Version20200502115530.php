<?php

declare(strict_types=1);

namespace App\Migrations;

use App\Entity\Account\AccountType;
use App\Entity\Transaction\TransactionType;
use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Init Database default values.
 */
final class Version20200502115530 extends AbstractMigration
{
    /**
     * @return string
     */
    public function getDescription(): string
    {
        return 'Init Database default values';
    }

    /**
     * @param Schema $schema
     */
    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->initAccountType();
        $this->initTransactionType();
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('TRUNCATE TABLE account_type');
        $this->addSql('TRUNCATE TABLE transaction_type');
    }

    /**
     * Create in database the different types of account.
     */
    private function initAccountType(): void
    {
        foreach (AccountType::getAccountsConstants() as $key => $value) {
            if ($name = AccountType::getAccountTypeLibelleWithCode($key)) {
                $this->addSql("INSERT INTO account_type (name,code, interest_percentage) VALUES ('{$name}' ,'{$key}', '{$value}')");
            }
        }
    }

    /**
     * Create in database the different types of transaction.
     */
    private function initTransactionType(): void
    {
        foreach (TransactionType::getDefaultValues() as $key => $value) {
            $this->addSql("INSERT INTO transaction_type (name,code) VALUES ('{$value}', '{$key}')");
        }
    }
}
