<?php

namespace App\DataFixtures;

use App\Entity\Account\Account;
use App\Entity\Account\AccountType;
use App\Entity\Transaction\Credit;
use App\Entity\Transaction\Debit;
use App\Entity\Transaction\TransactionType;
use App\Entity\User\User;
use App\Services\AccountType\AccountTypeSearchProvider;
use App\Services\TransactionType\TransactionTypeSearchProvider;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

/**
 * Class AccountFixtures.
 */
final class UserFixtures extends Fixture
{
    /**
     * @var TransactionTypeSearchProvider
     */
    private TransactionTypeSearchProvider $transactionTypeSearchProvider;

    /**
     * @var AccountTypeSearchProvider
     */
    private AccountTypeSearchProvider $accountTypeSearchProvider;

    /**
     * @var UserPasswordEncoderInterface
     */
    private UserPasswordEncoderInterface $userPasswordEncoder;

    /**
     * UserFixtures constructor.
     *
     * @param TransactionTypeSearchProvider $transactionTypeSearchProvider
     * @param AccountTypeSearchProvider     $accountTypeSearchProvider
     * @param UserPasswordEncoderInterface  $userPasswordEncoder
     */
    public function __construct(
        TransactionTypeSearchProvider $transactionTypeSearchProvider,
        AccountTypeSearchProvider $accountTypeSearchProvider,
        UserPasswordEncoderInterface $userPasswordEncoder
    ) {
        $this->transactionTypeSearchProvider = $transactionTypeSearchProvider;
        $this->accountTypeSearchProvider = $accountTypeSearchProvider;
        $this->userPasswordEncoder = $userPasswordEncoder;
    }

    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $faker = Factory::create();

        /** @var TransactionType[] $transactionTypes */
        $transactionTypes = $this->transactionTypeSearchProvider->findAll();
        /** @var AccountType[] $accountTypes */
        $accountTypes = $this->accountTypeSearchProvider->findAll();

        for ($i = 0; $i < 10; ++$i) {
            $user = (new User())->setEmail($faker->companyEmail);
            $user->setPassword($this->userPasswordEncoder->encodePassword($user, 'user'));
            foreach ($accountTypes as $accountType) {
                $account = new Account();
                $account->setAccountType($accountType);
                $account->setSolde($faker->randomFloat(2, 100, 1600));

                foreach ($transactionTypes as $transactionType) {
                    if (key_exists($transactionType->getCode(), TransactionType::getCreditTypes())) {
                        $transaction = new Credit();
                    } else {
                        $transaction = new Debit();
                    }

                    $transaction->setTransactionType($transactionType);
                    $transaction->setAmount($faker->randomFloat(2, 1, 150));
                    $transaction->setAccount($account);
                    $account->addTransaction($transaction);
                }

                $user->addAccount($account);
            }
            $manager->persist($user);
        }

        $manager->flush();
    }
}
