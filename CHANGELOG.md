# Changelog

Tous les changements notables à  ce projet seront documentés dans ce fichier.
Le format est basé sur [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
et ce projet adhère à  la version sémantique. [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [UNRELEASED]
### Added
 - Add base entities
 - Add ApiPlatform with configuration and access controls
 - Add customs data providers for subresources to control access
 - Add Symfony's security layer and JWT Token authentification
 - Rewrite of some Ylobank's services in PHP 7.4 
