<?php

namespace App\Services\AbstractServices;

use Doctrine\ORM\EntityRepository;

/**
 * Class BaseSearchProvider.
 */
abstract class BaseSearchProvider
{
    /**
     * @var EntityRepository
     */
    protected EntityRepository $entityRepository;

    /**
     * BaseSearchProvider constructor.
     *
     * @param EntityRepository $entityRepository
     */
    public function __construct(EntityRepository $entityRepository)
    {
        $this->entityRepository = $entityRepository;
    }

    /**
     * @return array
     */
    public function findAll(): array
    {
        return $this->entityRepository->findAll();
    }

    /**
     * @param int $id
     *
     * @return object|null
     */
    public function find(int $id): object
    {
        return $this->entityRepository->find($id);
    }
}
