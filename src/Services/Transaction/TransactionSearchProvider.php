<?php

namespace App\Services\Transaction;

use App\Entity\Account\Account;
use App\Entity\User\User;
use App\Repository\TransactionRepository;
use App\Services\AbstractServices\BaseSearchProvider;
use App\Services\Stats\Converter\DoughutDataConverter;
use DateTime;
use Exception;

/**
 * Class TransactionSearchProvider.
 */
final class TransactionSearchProvider extends BaseSearchProvider
{
    /**
     * @var DoughutDataConverter
     */
    private DoughutDataConverter $doughutDataConverter;

    /**
     * TransactionTypeSearchProvider constructor.
     *
     * @param TransactionRepository $transactionRepository
     * @param DoughutDataConverter  $doughutDataConverter
     */
    public function __construct(TransactionRepository $transactionRepository, DoughutDataConverter $doughutDataConverter)
    {
        parent::__construct($transactionRepository);
        $this->doughutDataConverter = $doughutDataConverter;
    }

    /**
     * @param User $user
     * @param bool $isIncome
     *
     * @return float
     *
     * @throws Exception
     */
    public function getMonthlyIncomeOfUser(User $user, $isIncome = true): float
    {
        /** @var TransactionRepository $er */
        $er = $this->entityRepository;

        return $er->getMonthlyIncomeOrOutcomeOfUser($user, $isIncome)->getSingleScalarResult();
    }

    /**
     * @param Account       $account
     * @param DateTime|null $periodStart
     * @param DateTime|null $periodEnd
     *
     * @return array
     */
    public function getSpendingsByCategoryInPeriodOfAccount(Account $account, ?DateTime $periodStart, ?DateTime $periodEnd): array
    {
        /** @var TransactionRepository $er */
        $er = $this->entityRepository;

        return $this->doughutDataConverter->convert($er->getSpendingsByCategoryInPeriodOfAccount($account, $periodStart, $periodEnd)->execute());
    }
}
