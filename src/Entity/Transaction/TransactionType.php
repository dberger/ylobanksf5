<?php

namespace App\Entity\Transaction;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class TransactionType.
 *
 * @ORM\Entity(repositoryClass="App\Repository\TransactionTypeRepository")
 */
class TransactionType
{
    const LIBELLE_RESTAURATION = 'Restauration';
    const LIBELLE_SHOPPING = 'Achats et Shopping';
    const LIBELLE_DIVERS = 'Divertissement';
    const LIBELLE_RETRAIT = 'Retrait';
    const LIBELLE_LOGEMENT = 'Logement';
    const LIBELLE_CREDIT = 'Crédit';
    const LIBELLE_CAR = 'Voiture';
    const LIBELLE_COURSES = 'Courses';
    const LIBELLE_SALAIRE = 'Salaire';
    const LIBELLE_HELP = 'Aide';
    const LIBELLE_VIREMENT = 'Virement';

    const CODE_RESTAURATION = 'restauration';
    const CODE_SHOPPING = 'achats_shopping';
    const CODE_DIVERS = 'divers';
    const CODE_RETRAIT = 'retrait';
    const CODE_LOGEMENT = 'logement';
    const CODE_CREDIT = 'credit';
    const CODE_CAR = 'car';
    const CODE_COURSES = 'courses';
    const CODE_SALAIRE = 'salaire';
    const CODE_HELP = 'help';
    const CODE_VIREMENT = 'virement';

    const SEARCH_CREDIT = 'search_credit';
    const SEARCH_DEBIT = 'search_debit';

    /**
     * @var int
     *
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Groups({"read_account"})
     */
    private int $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     * @Assert\NotNull()
     */
    private string $name;

    /**
     * @var string
     *
     * @ORM\Column(name="code", type="string", length=255)
     * @Assert\NotNull()
     * @Groups({"read_account"})
     */
    private string $code;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     *
     * @return TransactionType
     */
    public function setName(string $name): TransactionType
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return string
     */
    public function getCode(): string
    {
        return $this->code;
    }

    /**
     * @param string $code
     *
     * @return TransactionType
     */
    public function setCode(string $code): TransactionType
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Return an array of transaction's code as key with the linked libelle
     * Used for migrations.
     *
     * @return array
     */
    public static function getDefaultValues(): array
    {
        return [
            self::CODE_CAR => self::LIBELLE_CAR,
            self::CODE_COURSES => self::LIBELLE_COURSES,
            self::CODE_CREDIT => self::LIBELLE_CREDIT,
            self::CODE_DIVERS => self::LIBELLE_DIVERS,
            self::CODE_RESTAURATION => self::LIBELLE_RESTAURATION,
            self::CODE_LOGEMENT => self::LIBELLE_LOGEMENT,
            self::CODE_RETRAIT => self::LIBELLE_RETRAIT,
            self::CODE_SHOPPING => self::LIBELLE_SHOPPING,
            self::CODE_VIREMENT => self::LIBELLE_VIREMENT,
            self::CODE_SALAIRE => self::LIBELLE_SALAIRE,
            self::CODE_HELP => self::LIBELLE_HELP,
        ];
    }

    /**
     * Return an array of transaction's code as key with the linked libelle which are of credit types
     * Used for migrations.
     *
     * @return array
     */
    public static function getCreditTypes(): array
    {
        return [
            self::CODE_VIREMENT => self::LIBELLE_VIREMENT,
            self::CODE_SALAIRE => self::LIBELLE_SALAIRE,
            self::CODE_HELP => self::LIBELLE_HELP,
        ];
    }

    /**
     * Return an array of transaction's code as key with the linked libelle which are of debit types
     * Used for migrations.
     *
     * @return array
     */
    public static function getDebitTypes(): array
    {
        return [
            self::CODE_CAR => self::LIBELLE_CAR,
            self::CODE_COURSES => self::LIBELLE_COURSES,
            self::CODE_CREDIT => self::LIBELLE_CREDIT,
            self::CODE_DIVERS => self::LIBELLE_DIVERS,
            self::CODE_RESTAURATION => self::LIBELLE_RESTAURATION,
            self::CODE_LOGEMENT => self::LIBELLE_LOGEMENT,
            self::CODE_RETRAIT => self::LIBELLE_RETRAIT,
            self::CODE_SHOPPING => self::LIBELLE_SHOPPING,
        ];
    }
}
