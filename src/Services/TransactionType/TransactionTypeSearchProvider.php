<?php

namespace App\Services\TransactionType;

use App\Entity\Transaction\Transaction;
use App\Repository\TransactionTypeRepository;
use App\Services\AbstractServices\BaseSearchProvider;
use Doctrine\Common\Collections\Collection;

/**
 * Class TransactionTypeSearchProvider.
 */
final class TransactionTypeSearchProvider extends BaseSearchProvider
{
    /**
     * TransactionTypeSearchProvider constructor.
     *
     * @param TransactionTypeRepository $transactionTypeRepository
     */
    public function __construct(TransactionTypeRepository $transactionTypeRepository)
    {
        parent::__construct($transactionTypeRepository);
    }

    /**
     * Return only the objects that are for the debit form.
     *
     * @param string|null $type
     *
     * @return Collection|Transaction[]
     */
    public function getTransactionTypes(string $type = null): Collection
    {
        /** @var TransactionTypeRepository $repo */
        $repo = $this->entityRepository;

        return $repo->getTypes($type)->execute();
    }
}
