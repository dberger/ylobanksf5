<?php

namespace App\Services\Transaction;

use App\Entity\Account\Account;
use App\Entity\Transaction\Credit;
use App\Entity\Transaction\Debit;
use App\Entity\Transaction\Transaction;
use App\Entity\Transaction\TransactionType;
use Exception;

/**
 * Class TransactionFactory.
 */
final class TransactionFactory
{
    /**
     * @param Account         $account
     * @param TransactionType $transactionType
     * @param float           $amount
     *
     * @return Transaction
     *
     * @throws Exception
     */
    public function buildTransactionObject(Account $account, TransactionType $transactionType, float $amount): Transaction
    {
        if (key_exists($transactionType->getCode(), TransactionType::getCreditTypes())) {
            return (new Credit())->setAmount($amount)->setTransactionType($transactionType)->setAccount($account);
        }

        return (new Debit())->setAmount($amount)->setTransactionType($transactionType)->setAccount($account);
    }
}
