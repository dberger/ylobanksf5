<?php

namespace App\DataProvider\Account;

use ApiPlatform\Core\DataProvider\RestrictedDataProviderInterface;
use ApiPlatform\Core\DataProvider\SubresourceDataProviderInterface;
use App\Entity\Account\Account;
use App\Entity\User\User;
use App\Repository\AccountRepository;
use Doctrine\Common\Collections\Collection;
use Generator;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

/**
 * Class AccountSubresourceDataProvider.
 */
final class AccountSubresourceDataProvider implements SubresourceDataProviderInterface, RestrictedDataProviderInterface
{
    /**
     * @var AccountRepository
     */
    private AccountRepository $accountRepository;

    /**
     * @var TokenStorageInterface
     */
    private TokenStorageInterface $tokenStorage;

    public function __construct(AccountRepository $accountRepository, TokenStorageInterface $tokenStorage)
    {
        $this->accountRepository = $accountRepository;
        $this->tokenStorage = $tokenStorage;
    }

    /**
     * @param string $resourceClass
     * @param string|null $operationName
     * @param array $context
     *
     * @return bool
     */
    public function supports(string $resourceClass, string $operationName = null, array $context = []): bool
    {
        return Account::class === $resourceClass;
    }

    /**
     * @param string $resourceClass
     * @param array $identifiers
     * @param array $context
     * @param string|null $operationName
     *
     * @return Account[]|Collection
     */
    public function getSubresource(
        string $resourceClass,
        array $identifiers,
        array $context,
        string $operationName = null
    ): array
    {
        /** @var User $user */
        $user = $this->tokenStorage->getToken()->getUser();

        if (!$user instanceof User
            || ($user instanceof User && $user->getId() !== ($identifiers['id']['id'] ?? null))) {
            throw new AccessDeniedException('You must be logged in to access this subresource or you don\'t have permissions to access it.');
        }

        $query = $this
            ->accountRepository
            ->createQueryBuilder('a')
            ->andWhere('a.user = :user')
            ->setParameter('user', $user);

        return $query->getQuery()->getResult();
    }
}
