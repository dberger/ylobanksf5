<?php

namespace App\Entity\User;

use App\Entity\Account\Account;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 */
class User implements UserInterface
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private int $id;

    /**
     * @ORM\Column(type="string", length=180, unique=true)
     */
    private string $email;

    /**
     * @ORM\Column(type="json")
     */
    private array $roles = [];

    /**
     * @var string The hashed password
     * @ORM\Column(type="string")
     */
    private string $password;

    /**
     * @var Account[]|Collection
     * @ORM\OneToMany(targetEntity="App\Entity\Account\Account", mappedBy="user", cascade={"persist", "remove"}, orphanRemoval=true)
     */
    private Collection $accounts;

    /**
     * User constructor.
     */
    public function __construct()
    {
        $this->accounts = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUsername(): string
    {
        return (string) $this->email;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getPassword(): string
    {
        return (string) $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getSalt(): void
    {
        // not needed when using the "bcrypt" algorithm in security.yaml
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials(): void
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

    /**
     * @return Account[]|Collection
     */
    public function getAccounts(): Collection
    {
        return $this->accounts;
    }

    /**
     * @param Account $account
     *
     * @return User
     */
    public function addAccount(Account $account): User
    {
        if (!$this->accounts->contains($account)) {
            $account->setUser($this);
            $this->accounts->add($account);
        }

        return $this;
    }

    /**
     * @param Account $account
     *
     * @return User
     */
    public function removeAccount(Account $account): User
    {
        if (!$this->accounts->contains($account)) {
            $account->setUser(null);
            $this->accounts->add($account);
        }

        return $this;
    }

    /**
     * @param int $accountId
     *
     * @return bool
     */
    public function hasAccountId(int $accountId): bool
    {
        return $this->accounts->filter(fn (Account $account) => $account->getId() === $accountId)->count() > 0;
    }
}
