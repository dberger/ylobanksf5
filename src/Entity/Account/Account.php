<?php

namespace App\Entity\Account;

use App\Entity\Transaction\Credit;
use App\Entity\Transaction\Debit;
use App\Entity\Transaction\Transaction;
use App\Entity\User\User;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class Account.
 *
 * @ORM\Entity(repositoryClass="App\Repository\AccountRepository")
 */
class Account
{
    /**
     * @var int
     *
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Groups({"read_account"})
     */
    private int $id;

    /**
     * @var AccountType
     * @Assert\NotNull()
     * @ORM\ManyToOne(targetEntity="App\Entity\Account\AccountType")
     * @ORM\JoinColumn(name="id_account_type", referencedColumnName="id", nullable=false)
     * @Groups({"read_account"})
     */
    private AccountType $accountType;

    /**
     * @var float
     *
     * @ORM\Column(name="solde", type="decimal", precision=7, scale=2)
     * @Groups({"read_account"})
     */
    private float $solde = 0;

    /**
     * @var User
     * @Assert\NotNull()
     * @ORM\ManyToOne(targetEntity="App\Entity\User\User", inversedBy="accounts")
     * @ORM\JoinColumn(name="id_user", referencedColumnName="id", nullable=false)
     */
    private User $user;

    /**
     * @var Transaction[]|Collection
     * @ORM\OneToMany(targetEntity="App\Entity\Transaction\Transaction", mappedBy="account", cascade={"persist", "remove"})
     * @Groups({"read_account"})
     */
    private Collection $transactions;

    /**
     * Account constructor.
     */
    public function __construct()
    {
        $this->transactions = new ArrayCollection();
    }

    /**
     * @return AccountType
     */
    public function getAccountType(): AccountType
    {
        return $this->accountType;
    }

    /**
     * @param AccountType $accountType
     *
     * @return Account
     */
    public function setAccountType(AccountType $accountType): Account
    {
        $this->accountType = $accountType;

        return $this;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return Transaction[]|Collection
     */
    public function getTransactions(): Collection
    {
        return $this->transactions;
    }

    /**
     * @param Transaction[]|Collection $transactions
     *
     * @return Account
     */
    public function setTransactions(Collection $transactions): Account
    {
        $this->transactions = $transactions;

        return $this;
    }

    /**
     * Add a transaction.
     *
     * @param Transaction $transaction
     *
     * @return Account
     */
    public function addTransaction(Transaction $transaction): Account
    {
        if (!$this->transactions->contains($transaction)) {
            if ($transaction->getAccount() !== $this) {
                $transaction->setAccount($this);
            }
            $this->transactions->add($transaction);
            $this->solde = $transaction instanceof Credit ?
                $this->solde + $transaction->getAmount()
                : (
                    $transaction instanceof Debit ? $this->solde - $transaction->getAmount() : $this->solde
                );
        }

        return $this;
    }

    /**
     * Remove a transaction.
     *
     * @param Transaction $transaction
     *
     * @return Account
     */
    public function removeTransaction(Transaction $transaction): Account
    {
        if ($this->transactions->contains($transaction)) {
            $transaction->setAccount(null);
            $this->transactions->removeElement($transaction);
            $this->solde = $transaction instanceof Credit ?
                $this->solde - $transaction->getAmount()
                : (
                    $transaction instanceof Debit ? $this->solde + $transaction->getAmount() : $this->solde
                );
        }

        return $this;
    }

    /**
     * @return float
     */
    public function getSolde(): float
    {
        return $this->solde;
    }

    /**
     * @param float $solde
     *
     * @return Account
     */
    public function setSolde(float $solde): Account
    {
        $this->solde = $solde;

        return $this;
    }

    /**
     * @return User
     */
    public function getUser(): User
    {
        return $this->user;
    }

    /**
     * @param User $user
     *
     * @return Account
     */
    public function setUser(User $user): Account
    {
        $this->user = $user;

        return $this;
    }

    /**
     * @return string
     * @Groups({"read_account"})
     */
    public function getOwner() : string {
        return $this->getUser() instanceof User ? $this->getUser()->getUsername() : '';
    }

    /**
     * @return string
     * @Groups({"read_account"})
     */
    public function getAccountNumber() : string {
        return $this->getId();
    }
}
