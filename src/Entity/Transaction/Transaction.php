<?php

namespace App\Entity\Transaction;

use App\Entity\Account\Account;
use App\Entity\User\User;
use DateTime;
use Doctrine\ORM\Mapping as ORM;
use Exception;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class Transaction.
 *
 * @ORM\Entity(repositoryClass="App\Repository\TransactionRepository")
 * @ORM\Table(name="transaction",
 *     options={"comment":"Table of transactions"}
 * )
 * @ORM\InheritanceType("SINGLE_TABLE")
 * @ORM\DiscriminatorColumn(name="discr", type="string")
 * @ORM\DiscriminatorMap({
 *     "base" = "Transaction",
 *     "credit" = "Credit",
 *     "debit" = "Debit",
 *     })
 */
abstract class Transaction
{
    /**
     * @var integer
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer", options={"comment":"Id of transaction"})
     * @Groups({"read_account"})
     */
    private int $id;

    /**
     * @var float
     *
     * @ORM\Column(name="amount", type="decimal", precision=7, scale=2)
     * @Assert\NotNull()
     * @Groups({"read_account", "read_transaction", "write_transaction"})
     */
    private float $amount;

    /**
     * @var Account
     * @Assert\NotNull()
     * @ORM\ManyToOne(targetEntity="App\Entity\Account\Account", inversedBy="transactions")
     * @ORM\JoinColumn(name="id_account", referencedColumnName="id", nullable=false)
     * @Groups({"read_transaction", "write_transaction"})
     */
    private Account $account;

    /**
     * @var DateTime
     * @Assert\NotNull()
     * @ORM\Column(name="date_transaction", type="datetime")
     * @Groups({"read_account", "read_transaction", "write_transaction"})
     */
    private DateTime $dateTransaction;

    /**
     * @var TransactionType
     * @Assert\NotNull()
     * @ORM\ManyToOne(targetEntity="App\Entity\Transaction\TransactionType")
     * @ORM\JoinColumn(name="id_transaction_type", referencedColumnName="id", nullable=false)
     * @Groups({"read_account", "read_transaction", "write_transaction"})
     */
    private TransactionType $transactionType;

    /**
     * Transaction constructor.
     *
     * @throws Exception
     */
    public function __construct()
    {
        $this->dateTransaction = new DateTime();
    }

    /**
     * @return float
     */
    public function getAmount(): float
    {
        return $this->amount;
    }

    /**
     * @param float $amount
     *
     * @return Transaction
     */
    public function setAmount(float $amount): Transaction
    {
        $this->amount = $amount;

        return $this;
    }

    /**
     * @return Account
     */
    public function getAccount(): Account
    {
        return $this->account;
    }

    /**
     * @param Account $account
     *
     * @return Transaction
     */
    public function setAccount(Account $account): Transaction
    {
        $this->account = $account;
        if ($account instanceof Account) {
            $account->addTransaction($this);
        }

        return $this;
    }

    /**
     * @return integer
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return TransactionType
     */
    public function getTransactionType(): TransactionType
    {
        return $this->transactionType;
    }

    /**
     * @param TransactionType $transactionType
     *
     * @return Transaction
     */
    public function setTransactionType(TransactionType $transactionType): Transaction
    {
        $this->transactionType = $transactionType;

        return $this;
    }

    /**
     * @return DateTime
     */
    public function getDateTransaction(): DateTime
    {
        return $this->dateTransaction;
    }

    /**
     * @return User|null
     */
    public function getUser(): ?User
    {
        return $this->getAccount() instanceof Account ? $this->getAccount()->getUser() : null;
    }
}
